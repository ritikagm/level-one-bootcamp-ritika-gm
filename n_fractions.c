#include<stdio.h>
#include<stdlib.h>
#include<math.h>

struct fraction
{
int n,d;
};
typedef struct fraction fract;

fract input();
fract sum(fract frac1, fract frac2);
int gcd(int p, int q);
int output(fract s);

int main()
{
  int n,i;
  fract x,a;
  printf("Enter the number of terms:\n");
  scanf("%d",&n);
  fract f[n];
  for(i=0;i<n;i++)
  { 
    printf("Enter the fraction %d\n", i+1);
    f[i] = input();
  }
    if(n==1)
    {
     a=f[0];
    }
    else
    {
     x=f[0];
     for(i=0;i<n-1;i++)
     {
      a=sum(x,f[i+1]);
      x=a;
     }
   }
    output(a);
    return 0;
   }

fract input()
{
 fract f;
 printf("Numerator:");
 scanf("%d",&f.n);
 printf("Denominator:");
 scanf("%d",&f.d);
 return f;
}

int gcd(int p,int q)
{
 int i,a;
 for(i=1;i<=p && i<=q; i++)
  {
     if(p%i==0 && q%i==0)
       a=i;
 }
  return a;
}
 
fract sum(fract frac1, fract frac2)
{
  fract frac3;
  int c;
  frac3.n= ((frac1.n*frac2.d) + (frac2.n*frac1.d));
  frac3.d=frac1.d*frac2.d;
  c=gcd(frac3.n,frac3.d);
  frac3.n=frac3.n/c;
  frac3.d=frac3.d/c;
  return frac3;
}

int output(fract s)
{
  printf("The sum of given fractions is: %d/%d\n",s.n,s.d);
  return 0;
}
