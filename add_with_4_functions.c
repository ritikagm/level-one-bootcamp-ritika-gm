//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
int input()
{
  int n;
  printf("Enter the number\n");
  scanf("%d",&n);
  return n;
}
int sum(int a, int b)
{
  int sum;
  sum=a+b;
  return sum;
}
void output(int n)
{
  printf("The sum is %d\n",n);
}
int main()
{
  int x,y,z;
  x=input();
  y=input();
  z=sum(x,y);
  output(z);
  return 0;
}
