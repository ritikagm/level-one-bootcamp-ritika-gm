//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
int main()
{
  float x1,x2,y1,y2,d;
  float point1x();
  float point1y();
  float point2x();
  float point2y();
 
  x1=point1x();
  y1=point1y();
  x2=point2x();
  y2=point2y();
  d=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
  printf("Distance between the two points is %f",d);
  return 0;
}
float point1x()
{
  float n;
  printf("Enter the x co-ordinate of point one\n");
  scanf("%f",&n);
  return n;
}
float point1y()
{
  float n;
  printf(" Enter the y co-ordinate of point one\n");
  scanf("%f",&n);
  return n;
}
float point2x()
{
  float n;
  printf("Enter the x co-ordinate of point two\n");
  scanf("%f",&n);
  return n;
}
float point2y()
{
  float n;
  printf("Enter the y co-ordinate of point two\n");
  scanf("%f",&n);
  return n;
}
