//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct point
{
    float x, y;
};
 
float distance(struct point a, struct point b)
{
    float distance;
    distance = sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) *(a.y - b.y));
    return distance;
}
 
int main()
{
    struct point a, b;
    printf("Enter the x coordinate of point A\n");
    scanf("%f", &a.x);
    printf("Enter the y coordinate of point A\n");
    scanf("%f", &b.y);
    printf("Enter the x coordinate of point B\n");
    scanf("%f", &a.x);
    printf("Enter the y coordinate of point B\n");
    scanf("%f", &b.y);
    printf("The distance between two points A and B  is %f\n", distance(a, b));
    return 0;
}
