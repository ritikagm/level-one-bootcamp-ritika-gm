//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input()
{
  float n;
  printf("Enter the value\n");
  scanf("%f",&n);
  return n;
}
float volume(float h,float d,float b)
{
  float volume;
  volume=0.333*((h*d*b)+(d/b));
  return volume;
}
void output(float v)
{
  printf("The volume of tromboloid is %f\n",v);
}
int main()
{
  float h,d,b,v;
  h=input(h);
  d=input(d);
  b=input(b);
  v=volume(h,d,b);
  output(v);
  return 0;
}
