//WAP to find the sum of two fractions.
#include<stdio.h>

struct fract
{
  int n,d;
};
typedef struct fract Fract;
Fract compute_sum();
void output(Fract f1,Fract f2,Fract s);
Fract input()
{
  Fract x;
  printf("Enter the numer and denom values:\n");
  scanf("%d%d",&x.n,&x.d);
  return x;
}
int main()
{
  Fract f1,f2;
  f1=input();
  f2=input();
  Fract s = compute_sum(f1,f2);
  output(f1,f2,s);
}
int gcd(int n,int d )
{
  int temp;
   
  while(n!=0)
  {
      temp=n;
      n=d%n;
      d=temp;
  }
  return d;
  }
  Fract compute_sum(Fract f1,Fract f2)
  {
      Fract s;
      int n,d;
      n=((f1.n*f2.d)+(f2.n*f1.d));
      d=(f1.d*f2.d);
      int g = gcd(n,d);
      s.n=n/g;
      s.d=d/g;
      return s;
  }
  void output(Fract f1, Fract f2, Fract s)
  {
    printf("The sum of %d/%d and %d/%d is %d/%d", f1.n,f1.d,f2.n,f2.d,s.n,s.d);
  }
 
